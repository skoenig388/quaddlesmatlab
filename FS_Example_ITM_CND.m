%Test scripts for making StimDef(ITM) & TrialDef(CND) files


Stim_Path = 'D:\\QuaddleRepository_20180515\\';
num_trials = 100; %must be even number
num_distractor_Quaddles = 13;
spatial_scale = 0.05;

%% determine which features you want to use the search task
Quaddle_shapes = {'00','02','03'};
Quaddle_patterns = {'00','01','06'};
Quaddle_colors = {'7070014_5000000','7070194_5000000','7070014_7070014','7070194_7070194','6000000_6000000'};
color_paths = {'Patterned','Patterned','Patternless_70','Patternless_70',''};
Quaddle_textures = {'00'};
Quaddle_arms = {'00'};
Quaddle_armends = {'00'};
context_nums = [3,5];
xbounds = [-140:10:140]*spatial_scale;
ybounds = [[1:5:51]+10]*spatial_scale;
z = 2*spatial_scale;

target1 = 'S02_P01_C7070014_5000000_T00_A00_E00.fbx';
target2 = 'S03_P06_C7070194_5000000_T00_A00_E00.fbx';

target_index = NaN(1,2);

%% Generate all possible Quaddles that can be generated from the feature values above
Quaddle_names = {};
Quaddle_full_path = {};
count = 0;
for color = 1:length(Quaddle_colors)
    for pattern = 1:length(Quaddle_patterns)
        if strcmpi(Quaddle_patterns{pattern},'00') && ~strcmpi(Quaddle_colors{color}(1:7),Quaddle_colors{color}(9:end))
           continue 
        end
       if ~strcmpi(Quaddle_patterns{pattern},'00') && strcmpi(Quaddle_colors{color}(1:7),Quaddle_colors{color}(9:end))
           continue 
        end
        for shape = 1:length(Quaddle_shapes)
            for texture = 1:length(Quaddle_textures)
                for arm = 1:length(Quaddle_arms)
                    for arm_end = 1:length(Quaddle_armends)
                        count = count+1;
                        Quaddle_names{count} = ['S' Quaddle_shapes{shape} '_P' Quaddle_patterns{pattern} ...
                            '_C' Quaddle_colors{color} '_T' Quaddle_textures{texture} ...
                            '_A' Quaddle_arms{arm} '_E' Quaddle_armends{arm_end} '.fbx'];
                        
                        if strcmpi(color_paths{color},'Patterned')
                            Quaddle_full_path{count} =[Stim_Path color_paths{color} '_S' Quaddle_shapes{shape} '\\' Quaddle_names{count}];
                        elseif isempty(color_paths{color})
                            Quaddle_full_path{count} =[Stim_Path Quaddle_names{count}];
                        else
                            Quaddle_full_path{count} =[Stim_Path color_paths{color} '\\' Quaddle_names{count}];
                        end
                        
                        
                        
                        if strcmpi( Quaddle_names{count},target1)
                            target_index(1) = count;
                        elseif strcmpi( Quaddle_names{count},target2)
                            target_index(2) = count;
                        end
                    end
                end
            end
        end
    end
end

%% Psuedorandomize trial order
trialind = [ones(1,num_trials/2) 2*ones(1,num_trials/2)];
trialind = trialind(randperm(num_trials));

%% Generate all Possible Stimuli locations
xylocs = NaN(2,length(xbounds)*length(ybounds));
count = 0;
for x = 1:length(xbounds)
    for y = 1:length(ybounds)
        count = count+1;
        xylocs(1,count) = xbounds(x);
        xylocs(2,count) = ybounds(y);
    end
end
%%

header=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\t' 'SetLocation' '\t' 'SetRotation' '\n'];


fileID = fopen('QuaddleItm.txt','w');
fprintf(fileID,header);
item_num = 0;
item_locc_index = NaN(length(Quaddle_names),length(xylocs));
for Q = 1:length(Quaddle_names)
    for locs = 1:length(xylocs)
        item_num = item_num+1;
        item_locc_index(Q,locs) = item_num;
        if any(Q == target_index)
            str = [num2str(item_num) '\t' Quaddle_names{Q} '\t' Quaddle_full_path{Q} '\t' ...
                '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                num2str(xylocs(1,locs)) '\t'  num2str(xylocs(1,locs)) '\t'  num2str(z) '\t' ...
                '0' '\t' '0' '\t' '0' '\t' ...
                '1' '\t' '3' '\t' '9999' '\t' ...
                'true' '\t' 'true' '\t' 'false' '\n'];
        else
            str = [num2str(item_num) '\t' Quaddle_names{Q} '\t' Quaddle_full_path{Q} '\t' ...
                '0' '\t'  '1' '\t' '2' '\t' '3' '\t' '4' '\t' ...
                num2str(xylocs(1,locs)) '\t'  num2str(xylocs(2,locs)) '\t'  num2str(z) '\t' ...
                '0' '\t' '0' '\t' '0' '\t' ...
                '0' '\t' '0' '\t' '9999' '\t' ...
                'true' '\t' 'true' '\t' 'false' '\n'];
            
        end
        fprintf(fileID,str);
    end
end

fclose(fileID);
%%
% %%
%
title=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
    'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
    'Stim13' '\t'  'Stim14' '\n'];

fileID = fopen('QuaddleCND.txt','w');
fprintf(fileID,title);

valid_distractors = 1:length(Quaddle_names);
valid_distractors(target_index(1)) = [];
valid_distractors(target_index(2)) = [];

for t = 1:length(trialind)
    
    distractor_indexes = randperm(length(valid_distractors));
    location_indexes = randperm(length(xylocs));%includes location for target
    
    
    
    str1 = ['TrialNum' num2str(t) '\t' num2str(t) '\t'];
    str2 = ['ContextNum' num2str(context_nums(trialind(t))) '\t'  num2str(context_nums(trialind(t)))  '\t'];
    
    str3 = [num2str(item_locc_index(target_index(trialind(t)),location_indexes(1))) '\t'];
    
    str4 = [];
    for stim = 2:num_distractor_Quaddles+1
        str4= [str4  num2str(item_locc_index(distractor_indexes(stim-1),location_indexes(stim))) '\t'];
    end
    
    fprintf(fileID,[str1 str2 str3  str4 '\n']);
end

fclose(fileID);
