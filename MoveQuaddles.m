base_dir = 'C:\Users\seth.koenig\Desktop\QuaddleGenerator\savedobjects\';

names = dir(base_dir);

for n = 1:length(names)
    disp(['moving file# ' num2str(n)])
    if ~names(n).isdir
        oldname = [base_dir names(n).name];
        
        if ~isempty(strfind(names(n).name,'S00'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S00\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S01'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S01\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S02'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S02\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S03'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S03\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S04'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S04\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S05'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S05\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S06'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S06\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S07'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S07\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S08'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S08\' names(n).name];
        elseif ~isempty(strfind(names(n).name,'S09'))
            newname = ['C:\Users\seth.koenig\Desktop\Quaddles\Patterned_S09\' names(n).name];
        else
           error('unknown name') 
        end
        movefile(oldname,newname);
        
    end
end
oldname