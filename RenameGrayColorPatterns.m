base_dir = 'C:\Users\seth.koenig\Documents\3dsMax\QuaddleGenerator Scripts\Patterns and Object Table\Patterns and Colours\';

graynames =    {'2050000','80100000','50100000','2020000','100100000','7070000','5050000','6070000'};
newgraynames = {'2000000','8000000', '5000000', '2000000','9900000',  '7000000','5000000','6000000'};

graycount = zeros(1,length(graynames));
ls = dir(base_dir);
all_sub_dirs = [];
for l = 3:length(ls)
    if ls(l).isdir
        all_sub_dirs = [all_sub_dirs {ls(l).name}];
    end
end
%%
%for sub = length(all_sub_dirs)
    
    sub_dir = base_dir;%[base_dir all_sub_dirs{sub} '\'];
    disp(sub_dir);
    ls = dir(sub_dir);
    
    
    for l = 1:length(ls)
        filename = ls(l).name;
        if ~isempty(strfind(filename,'.png')) || ~isempty(strfind(filename,'.fbx'))
            if ~isempty(strfind(filename,'Colour')) && ~isempty(strfind(filename,'.png')) %%probably base pattern
                dash = strfind(filename,'_');
                parantho = strfind(filename,'(');
                paranthc = strfind(filename,')');
                if length(dash) > 1
                    error('What is more than 1 dash doing here')
                end
                colorname1 = filename(parantho(2)+1:dash-1);
                colorname2 = filename(dash+1:paranthc(2)-1);
                newcolorname = [];
                
                for g = 1:length(graynames)
                    if strcmpi(colorname1,graynames{g})
                        newcolorname = [newgraynames{g} '_'];
                        graycount(g) = graycount(g)+1;
                        break
                    else
                        if g == length(graynames)
                            newcolorname = [colorname1 '_'];
                        end
                    end
                end
                for g = 1:length(graynames)
                    if strcmpi(colorname2,graynames{g})
                        newcolorname = [newcolorname newgraynames{g}];
                        graycount(g) = graycount(g)+1;
                        break
                    else
                        if g == length(graynames)
                            newcolorname = [newcolorname colorname2];
                        end
                    end
                end
                
                new_filename = [filename(1:parantho(2)) newcolorname filename(paranthc(2):end)];
                if ~strcmpi(new_filename,filename)
                    movefile([sub_dir filename],[sub_dir new_filename]);
                end
            else %%probably the actual files
                dashes = strfind(filename,'_');
                Cs = strfind(filename,'C');
                if isempty(Cs)
                    continue
                end
                dashesGC = find(dashes > Cs);
                dashesGC = dashes(dashesGC(1:2));%first dash is between colors 2nd dash after colorname
                
                colorname1 = filename(Cs+1:dashesGC(1)-1);
                colorname2 = filename(dashesGC(1)+1: dashesGC(2)-1);
                
                newcolorname = [];
                
                for g = 1:length(graynames)
                    if strcmpi(colorname1,graynames{g})
                        newcolorname = [newgraynames{g} '_'];
                        graycount(g) = graycount(g)+1;
                        break
                    else
                        if g == length(graynames)
                            newcolorname = [colorname1 '_'];
                        end
                    end
                end
                for g = 1:length(graynames)
                    if strcmpi(colorname2,graynames{g})
                        newcolorname = [newcolorname newgraynames{g}];
                        graycount(g) = graycount(g)+1;
                        break
                    else
                        if g == length(graynames)
                            newcolorname = [newcolorname colorname2];
                        end
                    end
                end
                
                new_filename = [filename(1:Cs) newcolorname filename(dashesGC(2):end)];
                if length(new_filename) > 40
                    error('what what?')
                end
                if ~strcmpi(new_filename,filename)
                    movefile([sub_dir filename],[sub_dir new_filename]);
                end
            end
        end
    end
%end