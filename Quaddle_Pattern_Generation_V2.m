%Script used to generate most colors and patterns for quaddles
%written by Seth Konig 2/12/18 based on Marcus Watson's code but updated
% on 2/15/18 to use sine/cosine functions instead.
% Code also saves a few strings to text file so they can be used with the
% Quaddle generator scripts. Strings should be just copy and paste and will
% change depending on what you generate here.

clear,clc

save_folder = 'C:\Users\seth.koenig\Documents\Thilo Projects\Quaddles\Patterns\'; %where to save the color patterns

pattern_base = 'Pattern'; %for naming
color_base = 'Colour'; %for naming
pattern_size = 800;
numstripes = 4;%this is frequency, actually 2x this since 4 white & 4 black stripes, should be a factor of pattern_size
%note naming scheme doesn't account for numstripes!
Ncolors = 8;%do not change unknowingly, get 1 gray too of equeal brightness

%% Generate the isoluminant color schemes
%generate 3 different luminant color maps but keep chromacity
%maps 1 & 3 for for pattern mappes so that patterns are perceptually nice
%map 2 is for solid map with ~average brightness

%values should be between 0-99, technically 100 is the max but don't want to code for it
%brightness is equivalent to luminance
%chr is chromacity or colorfullness: 0 is gray & 100 is full color
brightness1 = 70;
brightness2 = 60; %average for solid maps
brightness3 = 50;
chr1 = 70;
chr2 = 60;
chr3 = 50;

%angle is the same for both colormaps if Ncolors is the same
[Quaddlecolormap1,~,colorangles1] = seths_quaddle_isomapsV2(Ncolors,brightness1,chr1);
[Quaddlecolormap2,~,colorangles2] = seths_quaddle_isomapsV2(Ncolors,brightness2,chr2);
[Quaddlecolormap3,~,colorangles3] = seths_quaddle_isomapsV2(Ncolors,brightness3,chr3);

%---Define Color combos that are perceptually good---%
colorcombos = triu(ones(length(Quaddlecolormap1)))-eye(length(Quaddlecolormap1));
id0 =  eye(length(Quaddlecolormap1));
id0 =  [id0(2:end,:); zeros(1,length(Quaddlecolormap1))];
colorcombos(id0 == 1) = 0;%remove colors right next to each other since perceptually similarish
colorcombos(1,8) = 0;%remove pink and red combo since colors space is circular
[colorcombo_ind1,colorcombo_ind3] = find(colorcombos);

%% Name Colors
%naming scheme is as follows [brightness chromacity colorangle]

colornames1 = cell(1,length(Quaddlecolormap1));
colornames2 = cell(1,length(Quaddlecolormap2));
colornames3 = cell(1,length(Quaddlecolormap2));
for c = 1:length(Quaddlecolormap1)
    if c < length(Quaddlecolormap1) %for all colors
        if colorangles1(c) < 10
            coloranglestr1 = ['00' num2str(colorangles1(c))];
        elseif colorangles1(c) < 100
            coloranglestr1 = ['0' num2str(colorangles1(c))];
        else
            coloranglestr1 = num2str(colorangles1(c));
        end
        
        if colorangles2(c) < 10
            coloranglestr2 = ['00' num2str(colorangles2(c))];
        elseif colorangles2(c) < 100
            coloranglestr2 = ['0' num2str(colorangles2(c))];
        else
            coloranglestr2 = num2str(colorangles2(c));
        end
        
        if colorangles3(c) < 10
            coloranglestr3 = ['00' num2str(colorangles3(c))];
        elseif colorangles3(c) < 100
            coloranglestr3 = ['0' num2str(colorangles3(c))];
        else
            coloranglestr3 = num2str(colorangles3(c));
        end
        
        if brightness1 < 10
            brightnessstr1 = ['0' num2str(brightness1)];
        else
            brightnessstr1 = num2str(brightness1);
        end
        
        if brightness2 < 10
            brightnessstr2 = ['0' num2str(brightness2)];
        else
            brightnessstr2 = num2str(brightness2);
        end
        
        if brightness3 < 10
            brightnessstr3 = ['0' num2str(brightness3)];
        else
            brightnessstr3 = num2str(brightness3);
        end
        
        if chr1 < 10
            chromastr1 = ['0' num2str(chr1)];
        else
            chromastr1 = num2str(chr1);
        end
        
        if chr2 < 10
            chromastr2 = ['0' num2str(chr1)];
        else
            chromastr2 = num2str(chr1);
        end
        
        if chr3 < 10
            chromastr3 = ['0' num2str(chr3)];
        else
            chromastr3 = num2str(chr3);
        end
        
        colornames1{c} = [brightnessstr1 chromastr1 coloranglestr1];
        colornames2{c} = [brightnessstr2 chromastr2 coloranglestr2];
        colornames3{c} = [brightnessstr3 chromastr3 coloranglestr3];
    else %for isoluminant gray
        if brightness1 < 10
            brightnessstr1 = ['0' num2str(brightness1)];
        else
            brightnessstr1 = num2str(brightness1);
        end
        
        if brightness2 < 10
            brightnessstr2 = ['0' num2str(brightness2)];
        else
            brightnessstr2 = num2str(brightness2);
        end
        
        if brightness3 < 10
            brightnessstr3 = ['0' num2str(brightness3)];
        else
            brightnessstr3 = num2str(brightness3);
        end
        
        colornames1{c} = [brightnessstr1 '00000'];
        colornames2{c} = [brightnessstr2 '00000'];
        colornames3{c} = [brightnessstr3 '00000'];
    end
end
%% Generate Pattern Maps
Allpatterns = {'Solid','VStripes','HStripes','DStripes','Diamond','Checkered','Wavey','Traingular','Lace','Flower'};
pattern_number = [0:1:length(Allpatterns)-1];
patternmaps = cell(1,length(Allpatterns));
for p = 1:length(Allpatterns)
    
    switch Allpatterns{p}
        
        case 'Solid'
            patternmaps{p} = ones(pattern_size,pattern_size);
            
        case 'VStripes'
            Matrix = zeros(pattern_size,pattern_size);
            for x = 1:pattern_size;
                for y = 1:pattern_size
                    if cos(2*pi/(pattern_size/numstripes)*y) > 0
                        Matrix(y,x) = 1;
                    end
                end
            end
            patternmaps{p} = Matrix;
            
        case 'HStripes'
            Matrix = zeros(pattern_size,pattern_size);
            for x = 1:pattern_size;
                for y = 1:pattern_size
                    if cos(2*pi/(pattern_size/numstripes)*x) > 0
                        Matrix(y,x) = 1;
                    end
                end
            end
            patternmaps{p} = Matrix;
            
        case 'DStripes'
            Matrix = zeros(pattern_size,pattern_size);
            for x = 1:pattern_size;
                for y = 1:pattern_size
                    if cos(2*pi/(pattern_size/numstripes*2)*(x+y)) > 0
                        Matrix(y,x) = 1;
                    end
                end
            end
            patternmaps{p} = Matrix;
            
        case 'Diamond'
            Matrix = zeros(pattern_size,pattern_size);
            for x = 1:pattern_size;
                for y = 1:pattern_size
                    if cos(2*pi/(pattern_size/numstripes/2)*y) > sin(2*pi/(pattern_size/numstripes/2)*x)
                        Matrix(y,x) = 1;
                    end
                end
            end
            patternmaps{p} = Matrix;
            
        case 'Checkered'
            Matrix = zeros(pattern_size,pattern_size);
            for x = 1:pattern_size;
                for y = 1:pattern_size
                    if cos(2*pi/(pattern_size/numstripes)*y)*sin(2*pi/(pattern_size/numstripes)*x) > 0
                        Matrix(y,x) = 1;
                    end
                end
            end
            patternmaps{p} = Matrix;
            
        case 'Wavey'
            stripe_width = size(Matrix,1)/numstripes/2;
            Matrix1 = zeros(stripe_width,pattern_size);
            Matrix2 = zeros(stripe_width,pattern_size);
            z = stripe_width/2*sin(2*pi/(pattern_size/numstripes)*(1:pattern_size));
            z1 = -stripe_width/2*sin(2*pi/(pattern_size/numstripes)*(1:pattern_size)-pi/(pattern_size/numstripes));
            for x = 1:pattern_size;
                val = z(x);
                base = stripe_width/2+1;
                for y = -stripe_width/2:stripe_width/2-1
                    if y < val;
                        Matrix1(y+base,x) = 1;
                    else
                        Matrix1(y+base,x) = 0;
                    end
                end
            end
            for x = 1:pattern_size;
                val = z1(x);
                base = stripe_width/2+1;
                for y = -stripe_width/2:stripe_width/2-1
                    if y < val;
                        Matrix2(y+base,x) = 1;
                    else
                        Matrix2(y+base,x) = 0;
                    end
                end
            end
            Matrix = zeros(stripe_width*numstripes,pattern_size);
            for stripe = 1:numstripes*2;
                ind = (stripe-1)*stripe_width+1:stripe*stripe_width;
                if rem(stripe,2) == 1
                    Matrix(ind,:) = Matrix1;
                else
                    Matrix(ind,:) = Matrix2(end:-1:1,:);
                end
            end
            patternmaps{p} = Matrix;
            
        case 'Traingular'
            %             Matrix = zeros(pattern_size,pattern_size);
            %             stripe_width = size(Matrix,1)/numstripes/2;
            %             Matrix1 = zeros(stripe_width,pattern_size);
            %             Matrix2 = zeros(stripe_width,pattern_size);
            %             z = stripe_width/2*triangle(2*pi/(pattern_size/numstripes)*(1:pattern_size));
            %             z1 = -stripe_width/2*triangle(2*pi/(pattern_size/numstripes)*(1:pattern_size)-pi/(pattern_size/numstripes));
            %             for x = 1:pattern_size;
            %                 val = z(x);
            %                 base = stripe_width/2+1;
            %                 for y = -stripe_width/2:stripe_width/2-1
            %                     if y < val;
            %                         Matrix1(y+base,x) = 1;
            %                     else
            %                         Matrix1(y+base,x) = 0;
            %                     end
            %                 end
            %             end
            %
            %             for x = 1:pattern_size;
            %                 val = z1(x);
            %                 base = stripe_width/2+1;
            %                 for y = -stripe_width/2:stripe_width/2-1
            %                     if y < val;
            %                         Matrix2(y+base,x) = 1;
            %                     else
            %                         Matrix2(y+base,x) = 0;
            %                     end
            %                 end
            %             end
            %             Matrix = zeros(stripe_width*numstripes,pattern_size);
            %             for stripe = 1:numstripes*2;
            %                 ind = (stripe-1)*stripe_width+1:stripe*stripe_width;
            %                 if rem(stripe,2) == 1
            %                     Matrix(ind,:) = Matrix1;
            %                 else
            %                     Matrix(ind,:) = Matrix2(end:-1:1,:);
            %                 end
            %             end
            
            Matrix = zeros(pattern_size,pattern_size);
            stripe_width = size(Matrix,1)/numstripes/2;
            Matrix1 = zeros(stripe_width,pattern_size);
            Matrix2 = zeros(stripe_width,pattern_size);
            z = stripe_width*triangle(2*pi/(pattern_size/numstripes)*(1:pattern_size));
            z1 = -stripe_width*triangle(2*pi/(pattern_size/numstripes)*(1:pattern_size)-pi/(pattern_size/numstripes));
            for x = 1:pattern_size;
                val = z(x);
                %base = stripe_width/2+1;
                %for y = -stripe_width/2:stripe_width/2-1
                base = 0;
                for y = 1:stripe_width;
                    if y < val;
                        Matrix1(y+base,x) = 1;
                    else
                        Matrix1(y+base,x) = 0;
                    end
                end
            end
            
            for x = 1:pattern_size;
                val = z1(x);
                %base = stripe_width/2+1;
                %for y = -stripe_width/2:stripe_width/2-1
                base = stripe_width;
                for y = -stripe_width+1:0
                    if y < val;
                        Matrix2(y+base,x) = 1;
                    else
                        Matrix2(y+base,x) = 0;
                    end
                end
            end
            
            for stripe = 1:numstripes*2;
                ind = (stripe-1)*stripe_width+1:stripe*stripe_width;
                if rem(stripe,2) == 1
                    Matrix(ind,:) = Matrix1;
                else
                    Matrix(ind,:) = Matrix2(end:-1:1,:);
                end
            end
            patternmaps{p} = Matrix;
            
        case 'Lace'
            Matrix = zeros(pattern_size,pattern_size);
            stripe_width = size(Matrix,1)/numstripes;
            z = stripe_width/2*sin(2*pi/(pattern_size/numstripes)*(1:pattern_size));
            for x = 1:pattern_size;
                val = z(x);
                for stripe = 1:numstripes
                    base = stripe_width*(stripe-1)+stripe_width/2+1;
                    for y = -stripe_width/2:stripe_width/2-1
                        if y < val;
                            Matrix(y+base,x) = 1;
                        else
                            Matrix(y+base,x) = 0;
                        end
                    end
                end
            end
            patternmaps{p} = Matrix;
            
        case 'Flower'
            mini_pattern_size = pattern_size/numstripes;
            Matrix = zeros(mini_pattern_size,mini_pattern_size);
            for x = 1:mini_pattern_size;
                for y = 1:mini_pattern_size;
                    R = sqrt((x-mini_pattern_size/2)^2+(y-mini_pattern_size/2)^2)*numstripes;
                    theta = atan2d(y-mini_pattern_size/2,x-mini_pattern_size/2);
                    if cosd(6*theta) > sind(R)
                        Matrix(y,x) = 1;
                    end
                end
            end
            Matrix = [[Matrix Matrix];[Matrix Matrix]];
            Matrix = [[Matrix Matrix];[Matrix Matrix]];
            patternmaps{p} = Matrix;
    end
end

%save('QuaddlePatterns&Colors','patternmaps','colorcombos','Quaddlecolormap','colornames','Allpatterns','pattern_size');
%clearvars -except patternmaps colorcombos Quaddlecolormap colornames Allpatterns pattern_size save_folder
%% Display Pattern Maps
figure
for p = 1:length(patternmaps)
    if length(patternmaps) <= 9
        subplot(3,3,p)
    elseif length(patternmaps) <= 16
        subplot(4,4,p)
    elseif length(patternmaps) <= 25
        subplot(5,5,p)
    end
    
    imagesc(patternmaps{p})
    axis square
    set(gca,'XTickLabel',[]);
    set(gca,'YTickLabel',[]);
    set(gca,'XTick',[]);
    set(gca,'YTick',[]);
    title(Allpatterns{p})
end
colormap('gray')

%% Generate Solid Color Patterns
p = 1;%for solid pattern;
%use map # 2 since average brightness
for i = 1:length(Quaddlecolormap1)
    
    colour1 = Quaddlecolormap1(i,:);
    
    coloredmap = zeros(pattern_size,pattern_size,3);
    map1 = patternmaps{p} == 1;
    map2 = patternmaps{p} == 0;
    for rgb  = 1:3
        temp = zeros(pattern_size,pattern_size);
        temp(map1) = colour1(rgb);
        coloredmap(:,:,rgb) = temp;
    end
    
    filename = [save_folder pattern_base '(' Allpatterns{1} ')+' color_base '(' ...
        colornames1{i} '_'  colornames1{i} ').png'];
    imwrite(coloredmap,filename)
end
%% Generate All Colour Patterns Patterns
for p = 2:length(patternmaps);%skip solid pattern #1
    for i = 1:length(colorcombo_ind1)
        
        colour1 = Quaddlecolormap1(colorcombo_ind1(i),:);
        colour3 = Quaddlecolormap3(colorcombo_ind3(i),:);
        
        coloredmap = zeros(pattern_size,pattern_size,3);
        map1 = patternmaps{p} == 1;
        map2 = patternmaps{p} == 0;
        for rgb  = 1:3
            temp = zeros(pattern_size,pattern_size);
            temp(map1) = colour1(rgb);
            temp(map2) = colour3(rgb);
            coloredmap(:,:,rgb) = temp;
        end
        
        coloredmap = imgaussfilt(coloredmap, 1);%makes it nicer to look at
        
        filename = [save_folder pattern_base '(' Allpatterns{p} ')+' color_base '(' ...
            colornames1{colorcombo_ind1(i)} '_'  colornames3{colorcombo_ind3(i)} ').png'];
        
        imwrite(coloredmap,filename)
    end
end

%% Generate Black & White Patterns...for display only?
colour1 = Quaddlecolormap1(end,:);%lighter
colour3 = Quaddlecolormap3(end,:);%darker
for p = 2:length(patternmaps);%skip solid
    coloredmap = zeros(pattern_size,pattern_size,3);
    map1 = patternmaps{p} == 1;
    map2 = patternmaps{p} == 0;
    for rgb  = 1:3
        temp = zeros(pattern_size,pattern_size);
        temp(map1) = colour1(rgb);
        temp(map2) = colour3(rgb);
        coloredmap(:,:,rgb) = temp;
    end
    
    coloredmap = imgaussfilt(coloredmap, 1);%makes it nicer to look at
    
    filename = [save_folder pattern_base '(' Allpatterns{p} ')+' color_base '(' ...
        colornames1{end} '_'  colornames3{end} ').png'];
    
    imwrite(coloredmap,filename)
end
%% Generate Text file that has color & pattern combos that were generated here
%just copy and paste in to quaddle generating script
%%
textfilename = [save_folder 'Strings4QuaddleGenerator.txt'];
fileID = fopen(textfilename,'w');
%%
%---For Solid Solors---%
fprintf(fileID,'For Solid Patterns \n','%s');
patternstring = 'pattern: \t #("Solid") \n';
fprintf(fileID,patternstring,'%s');
%%
colorstring = 'color: \t #(';
for c = 1:length(colornames1)
    if c == length(colornames1)
        colorstring = [colorstring '"' num2str(colornames2{c}) '_' num2str(colornames2{c}) '") \n'];
    else
        colorstring = [colorstring '"' num2str(colornames2{c}) '_' num2str(colornames2{c}) '",'];
    end
end
%%
fprintf(fileID,colorstring,'%s');
%%
%---For Color + Pattern---%
%fprintf(fileID,'\n \n For all Color and Pattern Combos \n','%s');
colorstring = 'color: \t #(';
for c = 1:length(colorcombo_ind1)
    if c == length(colorcombo_ind1)
        colorstring = [colorstring '"' num2str(colornames1{colorcombo_ind1(c)}) '_' num2str(colornames3{colorcombo_ind3(c)}) '") \n'];
    else
        colorstring = [colorstring '"' num2str(colornames1{colorcombo_ind1(c)}) '_' num2str(colornames3{colorcombo_ind3(c)}) '",'];
    end
end
%%
fprintf(fileID,colorstring,'%s');
patternstring = 'pattern: \t #(';
for p = 2:length(pattern_number)
    if p == length(pattern_number)
        patternstring = [patternstring '"' Allpatterns{p} '") \n'];
    else
        patternstring = [patternstring '"' Allpatterns{p} '",'];
    end
end
fprintf(fileID,patternstring,'%s');


%---For Black & White Patterns---%
fprintf(fileID,'\n \n For Black and White Patterns \n','%s');
colorstring = ['color: \t #("' num2str(colornames1{end}) '_' num2str(colornames3{end}) '") \n'];
fprintf(fileID,colorstring,'%s');
patternstring = 'pattern: \t #(';
for p = 2:length(pattern_number)
    if p == length(pattern_number)
        patternstring = [patternstring '"' Allpatterns{p} '") \n'];
    else
        patternstring = [patternstring '"' Allpatterns{p} '",'];
    end
end
fprintf(fileID,patternstring,'%s');

fclose(fileID);