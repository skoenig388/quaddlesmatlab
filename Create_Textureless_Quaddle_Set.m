%Determine, Find, and Copy "Skinny" subset of Quaddles (excludes textures)
%for monkeys FLU sets

clear,clc

stim_path = 'Z:\Resources\QuaddleRepository_20180515\';
new_location = 'Z:\Resources\TexturelessQuaddleRepo\';

current_dir = pwd;

shapes = {'S00','S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P00','P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

neutral_color =  'C6000000_6000000';
patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E00', 'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

Quaddle_count = 0;
% 
% %---Colored Patternless Quaddles----%
% for s =  1:length(shapes)
%     for p = 1
%         for c = 1:length(patternless_colors)
%             for a = 1:length(arms)
%                 Quaddle_name = [shapes{s} '_'  patterns{p} '_'  patternless_colors{c} '_' 'T00' '_' arms{a} '.fbx'];
%                 Quaddle_path = Determine_Quaddle_Path(Quaddle_name,stim_path);
%                 Quaddle_count = Quaddle_count+1;
%                 dahses = strfind(Quaddle_path,'\');
%                 new_path = Quaddle_path(dahses(3)+1:end-1);
%                 if ~exist([new_location new_path],'dir')
%                     mkdir([new_location new_path])
%                 end
%                 copyfile([Quaddle_path(1:end-1) Quaddle_name],[new_location new_path Quaddle_name]);
%             end
%         end
%     end
% end
% Quaddle_count
% 
% %---Patterned Gray Scale Quaddles---%
% for s =  1:length(shapes)
%     for p = 2:length(patterns)
%         for c = 1
%             for a = 1:length(arms)
%                 Quaddle_count = Quaddle_count+1;
%                 Quaddle_name = [shapes{s} '_'  patterns{p} '_'  gray_pattern_color '_' 'T00' '_' arms{a} '.fbx'];
%                 Quaddle_path = Determine_Quaddle_Path(Quaddle_name,stim_path);
%                 dahses = strfind(Quaddle_path,'\');
%                 new_path = Quaddle_path(dahses(3)+1:end-1);
%                 if ~exist([new_location new_path],'dir')
%                     mkdir([new_location new_path])
%                 end
%                 copyfile([Quaddle_path(1:end-1) Quaddle_name],[new_location new_path Quaddle_name]);
%                 
%             end
%         end
%     end
% end
% Quaddle_count
% 
% 
% %----Pattern + Color Quaddles
% for s =  1:length(shapes)
%     for p = 2:length(patterns)
%         for c = 1:length(patterned_colors)
%             for a = 1:length(arms)
%                 Quaddle_count = Quaddle_count+1;
%                 Quaddle_name = [shapes{s} '_'  patterns{p} '_'  patterned_colors{c} '_' 'T00' '_' arms{a} '.fbx'];
%                 Quaddle_path = Determine_Quaddle_Path(Quaddle_name,stim_path);
%                 dahses = strfind(Quaddle_path,'\');
%                 new_path = Quaddle_path(dahses(3)+1:end-1);
%                 if ~exist([new_location new_path],'dir')
%                     mkdir([new_location new_path])
%                 end
%                 copyfile([Quaddle_path(1:end-1) Quaddle_name],[new_location new_path Quaddle_name]);
%                 
%             end
%         end
%     end
% end
% Quaddle_count


%---Color less patternless Quaddles
for s =  1:length(shapes)
    for p = 1
        for c = 1
            for a = 1:length(arms)
                Quaddle_count = Quaddle_count+1;
                Quaddle_name = [shapes{s} '_'  patterns{p} '_'  neutral_color '_' 'T00' '_' arms{a} '.fbx'];
                Quaddle_path = Determine_Quaddle_Path(Quaddle_name,stim_path);
                dahses = strfind(Quaddle_path,'\');
                new_path = Quaddle_path(dahses(3)+1:end-1);
                if ~exist([new_location new_path],'dir')
                    mkdir([new_location new_path])
                end
                if isempty(new_path)
                    Quaddle_path = [Quaddle_path '\'];
                end
                copyfile([Quaddle_path(1:end-1) Quaddle_name],[new_location new_path Quaddle_name]);
            end
        end
    end
end
Quaddle_count