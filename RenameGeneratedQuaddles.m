%% Script renames files in selected folder using new naming convention

picture_folder = 'C:\Users\seth.koenig\Desktop\QuaddleGenerator\savedobjects\pictures\';
picture_destination_folder = 'C:\Users\seth.koenig\Desktop\QuaddleGenerator\savedobjects\';
fbx_folder = 'C:\Users\seth.koenig\Desktop\QuaddleGenerator\savedobjects\fbxFiles\';
destination_fbx_folder = 'C:\Users\seth.koenig\Desktop\QuaddleGenerator\savedobjects\';
%% Name Scheme is simply the index of the name in the following list...index starts with 00 and goes to 99

%Color names are already accounted for
Shape_names = {'Spherical','Pyramidal','Oblong','Compressed_Oblong','Octahedron','Cubic','Concave','Convex','Dumbbell','Pepper'};
Arm_angle_names = {'Straight','Bent_Up','Bent_Down'};
Arm_end_names = {'Flat','Flared','Blunt','Pointed'};
Smooth_names = {'Smooth','Wrinkled','Inward_Protrusions','Outward_Protrusions','Blocky','Hairy','Geodesic'};
Pattern_names = {'Solid','VStripes','HStripes','DStripes','Diamond','Checkered','Wavey','Traingular','Lace','Flower'};

%% Rename Picture Folder
names = dir([picture_folder '*.png']);
len  = length(names)
for n = 1:len
    filename = names(n).name;
    oldname = [picture_folder filename];
    pluses = strfind(filename,'+');
    
    pattern_str = [];
    color_str = [];
    shape_str = [];
    arm_str = [];
    arm_end_str = [];
    skin_texture_str = [];
    

    pattern_code = [];
    shape_code = [];
    arm_angle_code = [];
    arm_end_code = [];
    skin_code = [];
    for p = 1:length(pluses)+1
        if p == 1
            str_bit = filename(1:pluses(1)-1);
        elseif p == length(pluses)+1
            str_bit = filename(pluses(end)+1:end-4); %remove .png bit
        else
            str_bit = filename(pluses(p-1)+1:pluses(p)-1);
        end
        
        if ~isempty(strfind(str_bit,'Colour'))
            color_str = str_bit(8:end-1);
        elseif ~isempty(strfind(str_bit,'Body'))
            body_str = str_bit(6:end-1);
            body_code = find(ismember(Shape_names,body_str));
            if length(body_code) == 2 %should only happen because Oblong is in there 2x
                if strcmpi(body_str,'Oblong')
                    body_code = 3;
                else
                    error('Unknown string encountered');            
                end
            elseif length(body_code) > 1
                error('Unknown string encountered');                
            end
            body_code = body_code-1; 
        elseif ~isempty(strfind(str_bit,'Pattern'))
            pattern_str = str_bit(9:end-1);
            pattern_code = find(ismember(Pattern_names,pattern_str))-1;
        elseif ~isempty(strfind(str_bit,'Smoothness'))
            skin_texture_str = str_bit(12:end-1);
            skin_code =  find(ismember(Smooth_names,skin_texture_str))-1;
        elseif ~isempty(strfind(str_bit,'Arm_Ends'))
            arm_end_str = str_bit(10:end-1);
            arm_end_code =  find(ismember(Arm_end_names,arm_end_str))-1;
        elseif ~isempty(strfind(str_bit,'Arm_Angle'))
            arm_str = str_bit(11:end-1);
            arm_angle_code =  find(ismember(Arm_angle_names,arm_str))-1;
        else
            error('Unknown String')
        end
    end
    
    if skin_code < 10
        T_string = ['0' num2str(skin_code)]; 
    else
        T_string =  num2str(skin_code);
    end
    
    if arm_angle_code < 10
        A_string = ['0' num2str(arm_angle_code)]; 
    else
        A_string =  num2str(arm_angle_code);
    end
        
    if arm_end_code < 10
        E_string = ['0' num2str(arm_end_code)]; 
    else
        E_string =  num2str(arm_end_code);
    end
      
    if body_code < 10
        B_string = ['0' num2str(body_code)]; 
    else
        B_string =  num2str(body_code);
    end
    
    if pattern_code < 10
        P_string = ['0' num2str(pattern_code)]; 
    else
        P_string =  num2str(pattern_code);
    end
    
    if isempty(B_string) || isempty(P_string) || isempty(color_str) || isempty(T_string) || isempty(A_string) || isempty(E_string)
       error('Missing a code!') 
    end
    
    quaddle_name = ['S' B_string '_P' P_string '_C' color_str '_T' T_string '_A' A_string '_E' E_string];
        
    newname = [picture_destination_folder quaddle_name '.png'];
    movefile(oldname,newname);
end

%%
%% Rename FBX Folder
names = dir([fbx_folder '*.fbx']);
len  = length(names);
for n = 1:len
    filename = names(n).name;
    oldname = [fbx_folder filename];
    pluses = strfind(filename,'+');
    
    pattern_str = [];
    color_str = [];
    shape_str = [];
    arm_str = [];
    arm_end_str = [];
    skin_texture_str = [];
    
    pattern_code = [];
    shape_code = [];
    arm_angle_code = [];
    arm_end_code = [];
    skin_code = [];
    for p = 1:length(pluses)+1
        if p == 1
            str_bit = filename(1:pluses(1)-1);
        elseif p == length(pluses)+1
            str_bit = filename(pluses(end)+1:end-4); %remove .png bit
        else
            str_bit = filename(pluses(p-1)+1:pluses(p)-1);
        end
        
        if ~isempty(strfind(str_bit,'Colour'))
            color_str = str_bit(8:end-1);
        elseif ~isempty(strfind(str_bit,'Body'))
            body_str = str_bit(6:end-1);
            body_code = find(ismember(Shape_names,body_str));
            if length(body_code) == 2 %should only happen because Oblong is in there 2x
                if strcmpi(body_str,'Oblong')
                    body_code = 3;
                else
                    error('Unknown string encountered');            
                end
            elseif length(body_code) > 1
                error('Unknown string encountered');                
            end
            body_code = body_code-1; 
        elseif ~isempty(strfind(str_bit,'Pattern'))
            pattern_str = str_bit(9:end-1);
            pattern_code = find(ismember(Pattern_names,pattern_str))-1;
        elseif ~isempty(strfind(str_bit,'Smoothness'))
            skin_texture_str = str_bit(12:end-1);
            skin_code =  find(ismember(Smooth_names,skin_texture_str))-1;
        elseif ~isempty(strfind(str_bit,'Arm_Ends'))
            arm_end_str = str_bit(10:end-1);
            arm_end_code =  find(ismember(Arm_end_names,arm_end_str))-1;
        elseif ~isempty(strfind(str_bit,'Arm_Angle'))
            arm_str = str_bit(11:end-1);
            arm_angle_code =  find(ismember(Arm_angle_names,arm_str))-1;
        else
            error('Unknown String')
        end
    end
    
    if skin_code < 10
        T_string = ['0' num2str(skin_code)]; 
    else
        T_string =  num2str(skin_code);
    end
    
    if arm_angle_code < 10
        A_string = ['0' num2str(arm_angle_code)]; 
    else
        A_string =  num2str(arm_angle_code);
    end
        
    if arm_end_code < 10
        E_string = ['0' num2str(arm_end_code)]; 
    else
        E_string =  num2str(arm_end_code);
    end
      
    if body_code < 10
        B_string = ['0' num2str(body_code)]; 
    else
        B_string =  num2str(body_code);
    end
    
    if pattern_code < 10
        P_string = ['0' num2str(pattern_code)]; 
    else
        P_string =  num2str(pattern_code);
    end
    
    if isempty(B_string) || isempty(P_string) || isempty(color_str) || isempty(T_string) || isempty(A_string) || isempty(E_string)
       error('Missing a code!') 
    end
    
    quaddle_name = ['S' B_string '_P' P_string '_C' color_str '_T' T_string '_A' A_string '_E' E_string];
        
    newname = [destination_fbx_folder quaddle_name '.fbx'];
    movefile(oldname,newname);
end
newname