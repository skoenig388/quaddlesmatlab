%Check quaddles since missing a few...written 5/1/18 SDK
clar
main_Quaddle_dir = 'E:\';
%Color names are already accounted for
Shape_names = {'Spherical','Pyramidal','Oblong','Compressed_Oblong','Octahedron','Cubic','Concave','Convex','Dumbbell','Pepper'};
Arm_angle_names = {'Straight','Bent_Up','Bent_Down'};
Arm_end_names = {'Flat','Flared','Blunt','Pointed'};
Smooth_names = {'Smooth','Wrinkled','Inward_Protrusions','Outward_Protrusions','Blocky','Hairy','Geodesic'};
Pattern_names = {'Solid','VStripes','HStripes','DStripes','Diamond','Checkered','Wavey','Traingular','Lace','Flower'};


%% For Solid Color-Patterns...All Good
% 
% colornames = {'6070287_6070287','6070335_6070335','6070014_6070014','6070059_6070059',...
%     '6070106_6070106','6070148_6070148','6070194_6070194','6070239_6070239','6000000_6000000'};


%colornames = {'7070286_7070286','7070335_7070335','7070014_7070014','7070059_7070059','7070106_7070106',...
    %'7070148_7070148','7070194_7070194','7070240_7070240','7000000_7000000'};

% 
% subfolder = 'Patternless_60\';
% % 
% count = 0;
% does_not_exist_count = 0;
% for s = 1:length(Shape_names);
%     for aa = 1:length(Arm_angle_names)
%         for ae = 1:length(Arm_end_names)
%             for sm = 1:length(Smooth_names)
%                 for p = 1%:length(Pattern_names)
%                     for cl = 1:length(colornames)
%                           quaddle_name = ['S0' num2str(s-1) '_P0' num2str(p-1) '_C' colornames{cl}...
%                               '_T0' num2str(sm-1) '_A0' num2str(aa-1) '_E0' num2str(ae-1) '.fbx'];
%                           if ~exist([main_Quaddle_dir subfolder quaddle_name],'file')%% & sm ~= 1
%                               disp([quaddle_name ' does not exist'])
%                               does_not_exist_count = does_not_exist_count+1;
%                           end
%                         count = count+1;
%                     end
%                 end
%             end
%         end
%     end
% end

%% For All other Color-Patterns
% 

% colornames = {'7070286_5000000','7070335_5000000','7070014_5000000','7070059_5000000','7070106_5000000','7070148_5000000','7070194_5000000'};
% 
% subfolder_base = 'Patterned_S0';
% 
% count = 0;
% does_not_exist_count = 0;
% for s = 1:length(Shape_names);
%     for aa = 1:length(Arm_angle_names)
%         for ae = 1:length(Arm_end_names)
%             for sm = 1:length(Smooth_names)
%                 for p = 2:length(Pattern_names)
%                     for cl = 1:length(colornames)
%                         quaddle_name = ['S0' num2str(s-1) '_P0' num2str(p-1) '_C' colornames{cl}...
%                             '_T0' num2str(sm-1) '_A0' num2str(aa-1) '_E0' num2str(ae-1) '.fbx'];
%                         if ~exist([main_Quaddle_dir subfolder_base num2str(s-1) '\' quaddle_name],'file')
%                             if p ~= 6
%                                 disp([quaddle_name ' does not exist'])
%                                 does_not_exist_count = does_not_exist_count+1;
%                             end
%                         end
%                         count = count+1;
%                     end
%                 end
%             end
%         end
%     end
% end
%%
%% For Pattern Only

colornames = {'7000000_5000000'};

subfolder = 'Patterned_GrayScale\';

count = 0;
does_not_exist_count = 0;
for s = 1:length(Shape_names);
    for aa = 1:length(Arm_angle_names)
        for ae = 1:length(Arm_end_names)
            for sm = 1:length(Smooth_names)
                for p = 2:length(Pattern_names)
                    for cl = 1:length(colornames)
                          quaddle_name = ['S0' num2str(s-1) '_P0' num2str(p-1) '_C' colornames{cl}...
                              '_T0' num2str(sm-1) '_A0' num2str(aa-1) '_E0' num2str(ae-1) '.fbx'];
                          if ~exist([main_Quaddle_dir subfolder quaddle_name],'file')%% & sm ~= 1
                              disp([quaddle_name ' does not exist'])
                              does_not_exist_count = does_not_exist_count+1;
                          end
                        count = count+1;
                    end
                end
            end
        end
    end
end
