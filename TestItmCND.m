%Test scripts for making StimDef(ITM) & TrialDef(CND) files

title=['StimCode' '\t' 'StimName' '\t' 'StimPath' '\t' ...
    'StimDimVals1' '\t' 'StimDimVals2' '\t' 'StimDimVals3' '\t' 'StimDimVals4' '\t' 'StimDimVals5' '\t' ...
    'StimLocationX' '\t' 'StimLocationY' '\t' 'StimLocationZ' '\t' ...
    'StimRotationX' '\t' 'StimRotationY' '\t' 'StimRotationZ' '\t' ...
    'StimTrialRewardProb' '\t' 'StimTrialRewardMag' '\t' 'TimesUsedInBlock' ...
    '\t' 'isRelevant' '\n'];
    

fileID = fopen('QuaddleItm.txt','w');
fprintf(fileID,title);


Quaddle_base_name = 'Quaddle_Num';
num_Quaddles = 28;
for Q = 1:num_Quaddles
    
    str = [num2str(Q) '\t' [Quaddle_base_name '_Q' num2str(Q) '.fbx'] '\t' 'E:\\' '\t' ...
       '0' '\t'  '1' '\t' '1' '\t' '2' '\t' '6' '\t' ...
       num2str(randi([-15 15],1)) '\t'  num2str(randi([-15 15],1)) '\t'  num2str(randi([-15 15],1)) '\t' ...
       '0' '\t' '180' '\t' '90' '\t' ...
       '1' '\t' '1' '\t' '9999' '\t' 'true' '\n']; 

    fprintf(fileID,str);
end

fclose(fileID);

%%

title=['TrialName' '\t' 'TrialCode' '\t' 'ContextNum' '\t' 'ContextName' '\t' ...
    'Stim1' '\t'  'Stim2' '\t'  'Stim3' '\t'  'Stim4' '\t'  'Stim5' '\t'  'Stim6' '\t' ...
     'Stim7' '\t'  'Stim8' '\t'  'Stim9' '\t'  'Stim10' '\t'  'Stim11' '\t'  'Stim12' '\t' ...
     'Stim13' '\t'  'Stim14' '\n'];
    

fileID = fopen('QuaddleCND.txt','w');
fprintf(fileID,title);


num_Quaddles = 28;
trialcount = 101; 
for t = 1:trialcount
    
    str1 = ['TrialNum' num2str(t) '\t' num2str(t) '\t'];
    cntx = randi(3)-1;
    str2 = ['ContextNum' num2str(cntx) '\t' num2str(cntx) '\t'];
    
    str3 = [];
    randstr = randperm(num_Quaddles);
    for stim = 1:14
       str3 = [str3 num2str(randstr(stim)) '\t']; 
    end
    
    fprintf(fileID,[str1 str2 str3 '\n']);
end

fclose(fileID);
