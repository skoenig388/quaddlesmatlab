function [map,chr,colorangles] = seths_quaddle_isomapsV2(N,brightness,chr)
%will need colormap tool box by ?????
colourspace = 'LAB'; % Control points specified in CIELAB space
formula = 'CIE76';   % Default colour difference formula.
cyclic = 1; %since cyclic colormap, attributeStr = 'cyclic-isoluminant';
diagnostics = false;
W = [1 1 1]; %Default lightness correction
splineorder = 3;

%original values
%brightness = 67;%ranges from 0 to 100
%chr = 75;%was 42 sort of like radius

ang = 22.5;
colpts = [brightness  ch2ab(chr,  ang-90)
    brightness  ch2ab(chr,  ang)
    brightness  ch2ab(chr,  ang+90)
    brightness  ch2ab(chr,  ang+180)
    brightness  ch2ab(chr,  ang-90)];

labspline = pbspline(colpts',splineorder, N);
map = equalisecolourmap(colourspace, labspline', formula,W, 0, cyclic, diagnostics);
labspace = rgb2lab(map);%convert back to get colorangles, labspline does weird things but works out in the end
colorangles = round(atan2d(labspace(:,3),labspace(:,2)));%some tangential math
colorangles(colorangles < 0) = colorangles(colorangles < 0)+360;% so all values are postive

%add gray map
gray = zeros(4,3);
gray(:,1) = brightness;
graymap = equalisecolourmap(colourspace, gray, formula,W, 0, cyclic, diagnostics);
map = [map; graymap(1,:)];
colorangles = [colorangles; 0];
end

function ab = ch2ab(chroma, angle_degrees)
theta = angle_degrees/180*pi;
ab = chroma*[cos(theta) sin(theta)];
end