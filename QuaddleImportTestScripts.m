% test scripts for trying out different formats for importing Quaddles and
% other FBX files (optional)
% SDK June 19, 2018

Quaddle_names = {};
Quaddle_locations = {};
Quaddle_rotations = {};


itmfid = fopen('QuaddleItm.txt','w');
fprintf(itmfid,'ITM#\tQuaddleName\tXrot\tYrot\tZrot\n');
for Q = 1:15
    Quaddle_names{Q} = ['S0' num2str(Q) '_P0' num2str(Q) '.fbx'];
    Quaddle_rotations{Q}  = [0 180 90];
    fprintf(itmfid,[num2str(Q) '\t' Quaddle_names{Q} '\t' num2str(Quaddle_rotations{Q}(1)) ...
        '\t' num2str(Quaddle_rotations{Q}(2)) '\t' num2str(Quaddle_rotations{Q}(3)) '\n']);
end
fclose(itmfid);
%%

cndfid = fopen('QuaddleCND.txt','w');
fprintf(cndfid,'BLK#\tCND#\tITM#1\tITM#2\tITM#3\tITM#4\tITM#5\tITM#6\tITM#7\tITM#8\tITM#9\tITM#10\tITM#11\tITM#12\n')
for block = 1:5
    for trial = 1:10
        these_Quaddles = randperm(15);
        cndstr = [num2str(block) '\t' num2str(trial)];
        for item = 1:12
            cndstr = [cndstr '\t' num2str(these_Quaddles(item))];
        end
        fprintf(cndfid,[cndstr '\n'])
    end
end
fclose(cndfid);
